from django.shortcuts import render, get_object_or_404, redirect
from post.models import Post, Comment, Hashtag, Category, Review
from post.forms import PostForm, CommentForm


def post_list(request):
    posts = Post.objects.filter(draft=False)
    draft = Post.objects.filter(draft=True)
    draft_counter = len(draft)
    return render(request, 'post/post_list.html', {'posts': posts,
                                                   'drafts': draft,
                                                   'draft_counter': draft_counter})


def post_detail(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.counter += 1
    post.save()
    comments = Comment.objects.filter(post=post_pk)
    comments.counter = len(comments)
    reviews = Review.objects.filter(post=post_pk)
    estimate = 0
    if reviews:
        for i in reviews:
            estimate += i.mark
        post.rating = estimate/len(reviews)
        post.save()
    return render(request, 'post/post_detail.html', {'post': post, 'comments': comments})


def post_new(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = PostForm()
        return render(request, 'post/post_new.html', {'form': form})


def post_edit(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == 'POST':
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = PostForm(instance=post)
        return render(request, 'post/post_edit.html', {'form': form})


def post_delete(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.delete()
    return redirect('post_list')


def post_like(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.likes += 1
    post.save()
    return redirect('post_detail', post_pk=post.pk)


def post_dislike(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.dislikes += 1
    post.save()
    return redirect('post_detail', post_pk=post.pk)


def post_comment(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_form = form.save(commit=False)
            comment_form.author = request.user
            comment_form.post = post
            comment_form.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        comment_form = CommentForm()
        return render(request, 'post/post_comment.html', {'comment_form': comment_form})


def comment_delete(request, post_pk, comment_pk):
    post = get_object_or_404(Post, pk=post_pk)
    comment = get_object_or_404(Comment, pk=comment_pk)
    comment.delete()
    return redirect('post_detail', post_pk=post.pk)


def comment_edit(request, post_pk, comment_pk):
    post = get_object_or_404(Post, pk=post_pk)
    comment = get_object_or_404(Comment, pk=comment_pk)
    if request.method == 'POST':
        form = CommentForm(request.POST, instance=comment)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.save()
            return redirect('post_detail', post_pk=post.pk)
    else:
        form = CommentForm(instance=comment)
        return render(request, 'post/comment_edit.html', {'form': form})


def tags(request, tag_pk):
    tag = get_object_or_404(Hashtag, pk=tag_pk)
    posts = tag.POSTS.all()
    return render(request, 'post/post_list.html', {'posts': posts})


def draft_publish(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    post.draft = False
    post.save()
    return redirect('post_detail', post_pk=post.pk)


def show_category(request):
    return render(request, "post/categories.html", {'categories': Category.objects.all()})


# def categories(request):
#     categories = Category.objects.all()
#     return render(request, 'post/categories.html', {'categories': categories})


def post_in_category(request, category_pk):
    posts = Post.objects.filter(category=category_pk)
    draft = Post.objects.filter(draft=True)
    draft_counter = len(draft)
    return render(request, 'post/post_list.html', {'posts': posts,
                                                   'drafts': draft,
                                                   'draft_counter': draft_counter})


def rating(request):
    posts = Post.objects.all()
    lst = []
    for post in posts:
        if post.rating > 1.0 and post.likes > 10 and post.counter > 33:
            lst.append(post)

    return render(request, 'post/post_list.html', {'posts': lst})
