from django.contrib import admin
from post.models import Post, Comment, Hashtag, Category, Review
from mptt.admin import MPTTModelAdmin

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Hashtag)
admin.site.register(Category, MPTTModelAdmin)
admin.site.register(Review)
