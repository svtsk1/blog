from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from decimal import Decimal


class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=48)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    counter = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    image = models.ImageField(null=True, blank=True, upload_to='images/')
    tag = models.ManyToManyField('Hashtag', related_name='POSTS')
    draft = models.BooleanField(default=False)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, blank=True, null=True)
    rating = models.DecimalField(default=Decimal('3.00'), max_digits=3, decimal_places=2)

    def __str__(self):
        return f'{self.title}'


class Comment(models.Model):
    post = models.ForeignKey('Post', on_delete=models.CASCADE, blank=True, null=True)
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True, null=True)
    text = models.TextField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)
    counter = models.IntegerField(default=0)


class Hashtag(models.Model):
    text = models.CharField(max_length=33)

    def __str__(self):
        return self.text


class Category(MPTTModel):
    name = models.CharField(max_length=50, unique=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name


class Review(models.Model):
    mark = models.IntegerField()
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=True, blank=True)
    Text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank=True)
